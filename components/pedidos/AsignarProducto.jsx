import React, { useEffect, useState, useContext } from 'react';
import Select from 'react-select';
import { gql, useQuery } from "@apollo/client";
import PedidoContext from '../../context/pedidos/PedidoContext';

const OBTENER_PRODUCTOS = gql`
    query obtenerProductos{
        obtenerProductos{
            id
            nombre
            existencia
            precio
            creado
        }
    }
`;

const AsignarProducto = () => {

    const [productos, setProductos] = useState([]);

    const pedidoContext = useContext(PedidoContext);
    const { agregarProducto } = pedidoContext;

    const { data, loading } = useQuery(OBTENER_PRODUCTOS);

    useEffect(() => {
        agregarProducto(productos);
    }, [productos])

    const seleccionarProducto = productos => {
        setProductos(productos);
    }

    if (loading) return null;

    const { obtenerProductos } = data;

    return (
        <div>
            <p className="mt-10 my-2 bg-white border-l-4 border-gray-800 text-gray-700 p-2 text-sm font-bold">2.- Selecciona o busca los productos.</p> 
            <Select 
                className="mt-3"
                isMulti={true}
                options={obtenerProductos} 
                onChange={opcion => seleccionarProducto(opcion)}
                getOptionValue={opciones => opciones.id}
                getOptionLabel={opciones => `${opciones.nombre} - ${opciones.existencia} unidades disponibles`}
                placeholder="Busque o Seleccione un producto"
                noOptionsMessage={() => 'No hay resultados'} 
            />
        </div>
    )
}

export default AsignarProducto;
