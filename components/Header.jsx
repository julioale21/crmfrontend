import React from 'react';
import { useQuery, gql } from "@apollo/client";
import { useRouter } from "next/router";
import { OBTENER_USUARIO } from "../graphql/queries";

const Header = () => {
    const router = useRouter();
    const { data, loading } = useQuery(OBTENER_USUARIO);

    if(loading) return null;

    if(!data || !data.obtenerUsuario) {
        router.push('/login');
    }

    const { nombre, apellido } = data.obtenerUsuario;

    const cerrarSesion = () => {
        localStorage.removeItem('token');
        router.push('/login');
    }
    
    return (
        <div className="sm:flex sm:justify-between mb-6">
            <p className="mr-2 mb-5 lg:mb-0">Hola: {nombre} {apellido}</p>

            <button 
                type="button"
                className="bg-blue-800 w-full sm:w-auto font-bold uppercase text.xs rounded py-1 px-2 text-white shadow-md"
                onClick={() => cerrarSesion()}
            >
                Cerrar Sesión
            </button>
        </div>
        
    );
}

export default Header;