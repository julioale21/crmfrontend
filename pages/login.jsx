import { React, useState } from "react";
import Layout from '../components/Layout';
import { useFormik } from "formik";
import * as Yup from 'yup';
import { useMutation, gql } from "@apollo/client";
import { useRouter } from "next/router";
import MensajeError from '../components/MensajeError';
import { AUTENTICAR_USUARIO } from "../graphql/mutations";

const Login = () => {
    const router = useRouter();
    const [ autenticarUsuario ] = useMutation(AUTENTICAR_USUARIO);
    const [ mensaje, guardarMensaje ] = useState(null);

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string()
                        .required('El campo email es obligatorio')
                        .email('El email no es válido'),
            password: Yup.string()
                        .required('La contraseña es obligatoria')
                        .min(6, 'La contraseña debe contener al menos 6 caracteres')
        }),
        onSubmit: async (valores) => {

            const { email, password } = valores;

            try {
                const { data } = await autenticarUsuario({
                    variables: {
                        input: {
                            email,
                            password
                        }
                    }
                });
                
                guardarMensaje('Autenticando...');

                const { token } = data.autenticarUsuario;
                localStorage.setItem('token', token);

                setTimeout(() => {
                    guardarMensaje(null);
                    router.push('/');
                }, 2000);

            } catch (error) {
                guardarMensaje(error.message);

                setTimeout(() => {
                    guardarMensaje(null);
                },2000)
            }
        }
        
    });

    const mostrarMensaje = () => {
        return (
            <div 
                className="bg-white py-2 px-3 w-full my-3 max-w-sm text-center mx-auto"
            >
                <p>{mensaje}</p>
            </div>
        )
    }

    return (
        <>
            <Layout >
                <h1 className="text-center text-2xl text-white font-light">Login</h1>

                { mensaje && mostrarMensaje() }

                <div className="flex justify-center mt-5">
                    <div className="w-full max-w-sm">
                        <form 
                            className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4"
                            onSubmit={formik.handleSubmit}
                        >
                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">
                                    Email
                                </label>
                                <input 
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                    id="email"
                                    type="email"
                                    placeholder="Email Usuario"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.email}
                                />
                            </div>

                            { formik.touched.email && formik.errors.email ? (
                                <MensajeError mensaje={formik.errors.email} />
                            ) : null }

                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                                    Password
                                </label>
                                <input 
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id="password"
                                    type="password"
                                    placeholder="Contraseña"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.password}
                                />
                            </div>

                            { formik.touched.password && formik.errors.password ? (
                                <MensajeError mensaje={formik.errors.password} />
                            ) : null }

                            <input 
                                type="submit"
                                className="bg-gray-800 w-full mt-5 p-2 text-white uppercase hover:bg-gray-900"
                                value="Iniciar Sesión"
                            />
                        </form>
                    </div>
                </div>
            </Layout>
        </>   
    )
}

export default Login;