import React from 'react';
import Layout from '../../components/Layout';
import { useRouter } from "next/router";
import { Formik } from "formik";
import { gql, useQuery, useMutation } from '@apollo/client';
import * as Yup from 'Yup';
import Swal from "sweetalert2";
import MensajeError from '../../components/MensajeError';
import { OBTENER_PRODUCTO, OBTENER_PRODUCTOS } from "../../graphql/queries";
import { ACTUALIZAR_PRODUCTO } from "../../graphql/mutations";

const EditarProducto = () => {
    const router = useRouter();
    const { query: { id } } = router;

    const { data, loading } = useQuery(OBTENER_PRODUCTO, {
        variables: {
            id
        }
    });

    const [ actualizarProducto ] = useMutation(ACTUALIZAR_PRODUCTO, {
        update(cache, { data }) {
            try {
                const { actualizarProducto: productoActualizado } = data;
                const { obtenerProductos: productosActuales } = cache.readQuery({ query: OBTENER_PRODUCTOS});

                cache.writeQuery({
                    query: OBTENER_PRODUCTOS,
                    data: {
                        obtenerProductos: [...productosActuales, productoActualizado]
                    }
                });

                cache.writeQuery({
                    query: OBTENER_PRODUCTO,
                    variables: { id },
                    data: {
                        obtenerProducto: productoActualizado
                    }
                })
            } catch (error) {
                console.log(error);
            }
        }
    });


    const validationSchema = Yup.object({
        nombre: Yup.string()
                .required('El campo nombre es requerido'),
        precio: Yup.number()
                .required('El campo precio es requerido')
                .positive('El valor no puede ser negativo'),
        existencia: Yup.number()
                .required('El campo cantidad de producto es requerido')
                .positive('El valor no puede ser negativo')
                .integer('Debe ingresar un número entero'),
    });

    if(loading) return 'Cargando...';

    if(!data) return (
        <div className="flex justify-center mt-10">
            <h1 className="has-text-right text-2xl text-gray-800 font-light">Accion no permitida 🤔</h1>
        </div>
    )

    const { obtenerProducto } = data;

    const actualizarInfoProducto = async (valores) => {
        const { nombre, existencia, precio } = valores;

        try {
            const { data } = await actualizarProducto({
                variables: {
                    id,
                    input: {
                        nombre,
                        existencia, 
                        precio
                    }
                }
            });

            Swal.fire(
                'Actualizado',
                'El producto se actualizo correctamente',
                'success'
            );
            
            router.push('/productos');
        } catch (error) {
            console.error(error.message);
        }
    }

    return (
        <Layout>
            <h1 className="text-2xl text-gray-800 font-light">Editar Producto</h1>

            <div className="flex justify-center mt-5">
                <div className="w-full max-w-lg">

                <Formik 
                        enableReinitialize
                        initialValues={ obtenerProducto }
                        validationSchema={ validationSchema }
                        onSubmit={( valores ) => {
                            actualizarInfoProducto(valores)
                        }}
                    >

                    { props => {
                        return (
                            <form
                                className="bg-white shadow-md px-8 pt-6 pb-8 mb-4"
                                onSubmit={props.handleSubmit}
                            >
                                <div className="mb-4">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nombre">
                                        Nombre
                                    </label>
                                    <input
                                        className="shadow appearance-none border rounded w-full py-2 px-3 lg:text-gray-700 leading-tight  focus:shadow-outline"
                                        id="nombre"
                                        type="text"
                                        placeholder="Nombre Producto"
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        value={props.values.nombre}
                                    >
                                    </input>
                                </div>

                                {props.touched.nombre && props.errors.nombre ? (
                                    <MensajeError mensaje={props.errors.nombre}/>
                                ): null}

                                <div className="mb-4">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="existencia">
                                        Cantidad Disponible
                                    </label>
                                    <input
                                        className="shadow appearance-none border rounded w-full py-2 px-3 lg:text-gray-700 leading-tight  focus:shadow-outline"
                                        id="existencia"
                                        type="number"
                                        placeholder="Cantidad Disponible"
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        value={props.values.existencia}
                                    >
                                    </input>
                                </div>

                                {props.touched.existencia && props.errors.existencia ? (
                                    <MensajeError mensaje={props.errors.existencia}/>
                                ): null}

                                <div className="mb-4">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="precio">
                                        Precio
                                    </label>
                                    <input
                                        className="shadow appearance-none border rounded w-full py-2 px-3 lg:text-gray-700 leading-tight  focus:shadow-outline"
                                        id="precio"
                                        type="number"
                                        placeholder="Precio Producto"
                                        onChange={props.handleChange}
                                        onBlur={props.handleBlur}
                                        value={props.values.precio}
                                    >
                                    </input>
                                </div>

                                {props.touched.precio && props.errors.precio ? (
                                    <MensajeError mensaje={props.errors.precio}/>
                                ): null}

                                <input
                                    type="submit"
                                    className="bg-gray-800 w-full mt-5 p-2 text-white uppercase font-bold hover:bg-gray-900"
                                    value="Guardar Cambios" 
                                />

                            </form>
                        )
                    }}
                    </Formik>
                </div>
            </div>
        </Layout>
    )
}

export default EditarProducto;
