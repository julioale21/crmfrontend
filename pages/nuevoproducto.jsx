import { gql, useMutation } from '@apollo/client';
import React from 'react';
import Layout from '../components/Layout';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import MensajeError from '../components/MensajeError';
import { NUEVO_PRODUCTO } from "../graphql/mutations";
import { OBTENER_PRODUCTOS } from "../graphql/queries";


const NuevoProducto = () => {

    const router = useRouter();

    const [ nuevoProducto ] = useMutation(NUEVO_PRODUCTO, {
        update(cache, { data: { nuevoProducto }}) {
            try {
                const { obtenerProductos } = cache.readQuery({query: OBTENER_PRODUCTOS});

                cache.writeQuery({
                    query: OBTENER_PRODUCTOS,
                    data: {
                        obtenerProductos: [...obtenerProductos, nuevoProducto]
                    }
                })

                Swal.fire(
                    'Producto Creado',
                    'El producto se creó correctamente',
                    "success"
                )

                router.push('/productos');
            } catch (error) {
                console.log('La consulta no se encuentra en cache');
                return
            } 
        }
    });

    const formik = useFormik({
        initialValues: {
            nombre: '',
            existencia: '',
            precio: ''
        },
        validationSchema: Yup.object({
            nombre: Yup.string()
                .required('El campo nombre es requerido'),
            existencia: Yup.number()
                .required('El campo cantidad de producto es requerido')
                .positive('El valor no puede ser negativo')
                .integer('Debe ingresar un número entero'),
            precio: Yup.number()
                .required('El campo precio es requerido')
                .positive('El valor no puede ser negativo')
        }),
        onSubmit: async valores => {

            const { nombre, existencia, precio } = valores;

            try {
                const { data } = await nuevoProducto({
                    variables: {
                        input: {
                            nombre,
                            existencia,
                            precio
                        }
                    }
                })  
            } catch (error) {
                console.log(error);
            }
        }
    })

    return (
        <Layout>
            <h1 className="text-2xl text-gray-800 font-light">Crear Nuevo Producto</h1>

            <div className="flex justify-center mt-5">
                <div className="w-full max-w-lg">
                    <form
                        className="bg-white shadow-md px-8 pt-6 pb-8 mb-4"
                        onSubmit={formik.handleSubmit}
                    >
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nombre">
                                Nombre
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 lg:text-gray-700 leading-tight  focus:shadow-outline"
                                id="nombre"
                                type="text"
                                placeholder="Nombre Producto"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.nombre}
                            >
                            </input>
                        </div>

                        {formik.touched.nombre && formik.errors.nombre ? (
                            <MensajeError mensaje={formik.errors.nombre}/>
                        ): null}

                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="existencia">
                                Cantidad Disponible
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 lg:text-gray-700 leading-tight  focus:shadow-outline"
                                id="existencia"
                                type="number"
                                placeholder="Cantidad Disponible"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.existencia}
                            >
                            </input>
                        </div>

                        {formik.touched.existencia && formik.errors.existencia ? (
                            <MensajeError mensaje={formik.errors.existencia}/>
                        ): null}

                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="precio">
                                Precio
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 lg:text-gray-700 leading-tight  focus:shadow-outline"
                                id="precio"
                                type="number"
                                placeholder="Precio Producto"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.precio}
                            >
                            </input>
                        </div>

                        {formik.touched.precio && formik.errors.precio ? (
                            <MensajeError mensaje={formik.errors.precio}/>
                        ): null}

                        <input
                            type="submit"
                            className="bg-gray-800 w-full mt-5 p-2 text-white uppercase font-bold hover:bg-gray-900"
                            value="Agregar Nuevo Producto" 
                        />

                    </form>
                </div>
            </div>
        </Layout>
    )
}

export default NuevoProducto
