import React, { useState } from 'react';
import Layout from '../components/Layout';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import MensajeError from '../components/MensajeError';
import { gql, useMutation } from '@apollo/client';
import { useRouter } from "next/router";
import { NUEVO_CLIENTE } from "../graphql/mutations";
import { OBTENER_CLIENTES_USUARIO } from "../graphql/queries";


const NuevoCliente = () => {

    const [mensaje, setMensaje] = useState(null);
    const router = useRouter();

    const [nuevoCliente] = useMutation(NUEVO_CLIENTE, {
        update(cache, { data: { nuevoCliente }}) {
            const { obtenerClientesVendedor } = cache.readQuery({ query: OBTENER_CLIENTES_USUARIO})
            
            cache.writeQuery({
                query: OBTENER_CLIENTES_USUARIO,
                data: {
                    obtenerClientesVendedor: [...obtenerClientesVendedor, nuevoCliente]
                }
            })
        }
    });

    const formik = useFormik({
        initialValues: {
            nombre: '',
            apellido: '',
            empresa: '',
            email: '',
            telefono: ''
        },
        validationSchema: Yup.object({
            nombre: Yup.string()
                        .required('El nombre del cliente es obligatorio'),
            apellido: Yup.string()
                        .required('El apellido del cliente es obligatorio'),
            empresa: Yup.string()
                        .required('La empresa del cliente es obligatoria'),
            email: Yup.string()
                        .required('El email del cliente es obligatorio')
                        .email('El email no es válido'),
        }),
        onSubmit: async valores => {
            const { nombre, apellido, empresa, email, telefono } = valores;

            try {
                const { data } = await nuevoCliente({
                    variables: {
                        input: {
                            nombre, 
                            apellido,
                            empresa,
                            email,
                            telefono
                        }
                    }
                })

                setMensaje(`Se creo correctamente el cliente ${nombre} ${apellido}`);
                setTimeout(() => {
                    setMensaje(null);
                    router.push('/');
                }, 2000);
            
            } catch (error) {
                setMensaje(error.message);
                setTimeout(() => {
                    setMensaje(null)
                }, 2000)   
            }   
        }
    });

    const mostrarMensaje = () => {
        return (
            <div 
                className="bg-white py-2 px-3 w-full my-3 max-w-sm text-center mx-auto"
            >
                <p>{mensaje}</p>
            </div>
        )
    }

    return (
        <>
            <Layout>
                <div>
                    <h1 className="text-2xl text-gray-800 font-light">Nuevo Cliente</h1>

                    { mensaje && mostrarMensaje() }
                    
                    <div className="flex justify-center mt-5">
                        <div className="w-full max-w-lg">
                            <form 
                                className="bg-white shadow-md px-8 pt-6 pb-8 mb-4"
                                onSubmit={formik.handleSubmit}
                            >
                                <div className="mb-4">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nombre">
                                        Nombre
                                    </label>
                                    <input 
                                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                        id="nombre"
                                        type="text"
                                        placeholder="Nombre Cliente"
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        value={formik.values.nombre} 
                                    />
                                </div>

                                {formik.touched.nombre && formik.errors.nombre ? (
                                    <MensajeError mensaje={formik.errors.nombre}/>
                                ): null}

                                <div className="mb-4">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="apellido">
                                        Apellido
                                    </label>
                                    <input 
                                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                        id="apellido"
                                        type="text"
                                        placeholder="Apellido Cliente"
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        value={formik.values.apellido} 
                                    />
                                </div>

                                {formik.touched.apellido && formik.errors.apellido ? (
                                    <MensajeError mensaje={formik.errors.apellido}/>
                                ): null}

                                <div className="mb-4">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="empresa">
                                        Empresa
                                    </label>
                                    <input 
                                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                        id="empresa"
                                        type="text"
                                        placeholder="Empresa Cliente"
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        value={formik.values.empresa} 
                                    />
                                </div>

                                {formik.touched.empresa && formik.errors.empresa ? (
                                    <MensajeError mensaje={formik.errors.empresa}/>
                                ): null}

                                <div className="mb-4">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">
                                        Email
                                    </label>
                                    <input 
                                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                        id="email"
                                        type="email"
                                        placeholder="Email Cliente"
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        value={formik.values.email} 
                                    />
                                </div>

                                {formik.touched.email && formik.errors.email ? (
                                    <MensajeError mensaje={formik.errors.email}/>
                                ): null}

                                <div className="mb-4">
                                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="telefono">
                                        Teléfono
                                    </label>
                                    <input 
                                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                        id="telefono"
                                        type="tel"
                                        placeholder="Teléfono Cliente"
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        value={formik.values.telefono}
                                    />
                                </div>

                                <input
                                    type="submit"
                                    className="bg-gray-800 w-full mt-5 p-2 text-white uppercase font-bold hover:bg-gray-900"
                                    value="Registrar Cliente" 
                                />

                            </form>
                        </div>
                    </div>
                </div>
            </Layout>
        </>
    );
}

export default NuevoCliente;