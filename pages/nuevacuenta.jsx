import { React, useState } from "react";
import { useRouter } from "next/router";
import Layout from '../components/Layout';
import { useFormik } from "formik";
import * as Yup from 'yup';
import MensajeError from '../components/MensajeError';
import { useMutation, gql } from "@apollo/client";
import { NUEVA_CUENTA } from "../graphql/mutations";


const NuevaCuenta = () => {
    const [mensaje, guardarMensaje] = useState(null);
    const [ nuevoUsuario ] = useMutation(NUEVA_CUENTA);
    const router = useRouter();
    
    const formik = useFormik({
        initialValues: {
            nombre: '',
            apellido: '',
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            nombre: Yup.string()
                        .required('El nombre es obligatorio'),
            apellido: Yup.string()
                        .required('El apellido es obligatorio'),
            email: Yup.string()
                        .email('El email no es válido')
                        .required('El email es obligatorio'),
            password: Yup.string()
                        .required('La contraseña es obligatoria')
                        .min(6, 'La contraseña debe tener al menos 6 caracteres')
        }),
        onSubmit: async valores => {

            const { nombre, apellido, email, password } = valores;

            try {
                const { data } = await nuevoUsuario({
                    variables: {
                        input: {
                            nombre, 
                            apellido,
                            email,
                            password
                        }
                    }
                });

                guardarMensaje(`Se creo correctamente el Usuario: ${data.nuevoUsuario.nombre} ${data.nuevoUsuario.apellido}`);
                
                setTimeout(() =>{
                    guardarMensaje(null);
                    router.push('/login');
                }, 3000);



            } catch (error) {
                guardarMensaje(error.message);
                
                setTimeout(() => {
                    guardarMensaje(null);
                }, 3000);
            }       
        }
    });

    const mostrarMensaje = () => {
        return (
            <div 
                className="bg-white py-2 px-3 w-full my-3 max-w-sm text-center mx-auto"
            >
                <p>{mensaje}</p>
            </div>
        )
    }

    return (
        <>
            <Layout > 
                <h1 className="text-center text-2xl text-white font-light">Crear Nueva Cuenta</h1>

                { mensaje && mostrarMensaje() }

                <div className="flex justify-center mt-5">
                    <div className="w-full max-w-sm">
                        <form 
                            className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4"
                            onSubmit={formik.handleSubmit}
                        >
                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nombre">
                                    Nombre
                                </label>
                                <input 
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                    id="nombre"
                                    type="text"
                                    placeholder="Nombre Usuario"
                                    value={formik.values.nombre}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                />
                            </div>

                            { formik.touched.nombre && formik.errors.nombre ? (
                                <MensajeError mensaje={formik.errors.nombre} />
                            ) : null }

                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="apellido">
                                    Apellido
                                </label>
                                <input 
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                    id="apellido"
                                    type="text"
                                    placeholder="Apellido Usuario"
                                    value={formik.values.apellido}
                                    onChange={formik.handleChange}
                                />
                            </div>

                            { formik.touched.apellido && formik.errors.apellido ? (
                                <MensajeError mensaje={formik.errors.apellido} />
                            ) : null }

                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">
                                    Email
                                </label>
                                <input 
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                    id="email"
                                    type="email"
                                    placeholder="Email Usuario"
                                    value={formik.values.email}
                                    onChange={formik.handleChange}
                                />
                            </div>

                            { formik.touched.email && formik.errors.email ? (
                                <MensajeError mensaje={formik.errors.email} />
                            ) : null }


                            <div className="mb-4">
                                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                                    Password
                                </label>
                                <input 
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id="password"
                                    type="password"
                                    placeholder="Contraseña"
                                    value={formik.values.password}
                                    onChange={formik.handleChange}
                                />
                            </div>

                            { formik.touched.password && formik.errors.password ? (
                                <MensajeError mensaje={formik.errors.password} />
                            ) : null }

                            <input 
                                type="submit"
                                className="bg-gray-800 w-full mt-5 p-2 text-white uppercase hover:bg-gray-900"
                                value="Crear Cuenta"
                            />
                        </form>
                    </div>
                </div>
            </Layout>
        </>   
    )
}

export default NuevaCuenta;