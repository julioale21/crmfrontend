import React, { useContext, useState } from 'react';
import Layout from '../components/Layout';
import AsignarCliente from '../components/pedidos/AsignarCliente';
import AsignarProducto from '../components/pedidos/AsignarProducto';
import ResumenPedido from '../components/pedidos/ResumenPedido';
import PedidoContext from '../context/pedidos/PedidoContext';
import Total from '../components/pedidos/Total';
import { useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import Swal from 'sweetalert2';
import { OBTENER_PEDIDOS } from "../graphql/queries";
import { NUEVO_PEDIDO } from "../graphql/mutations";

const NuevoPedido = () => {
    const router = useRouter();
    const [mensaje, setMensaje] = useState(null);
    const pedidoContext = useContext(PedidoContext);
    const { cliente, productos, total } = pedidoContext;
    const [nuevoPedido] = useMutation(NUEVO_PEDIDO, {
        update(cache, { data: { nuevoPedido } }) {
            const { obtenerPedidosVendedor } = cache.readQuery({ query: OBTENER_PEDIDOS });

            cache.writeQuery({
                query: OBTENER_PEDIDOS,
                data: {
                    obtenerPedidosVendedor: [...obtenerPedidosVendedor, nuevoPedido]
                }
            })
        }
    });

    const validarPedido = () => {
        return !productos.every( producto => producto.cantidad > 0) || total === 0 || cliente.length === 0 ? " opacity-50 cursor-not-allowed" : ""
    }

    const crearNuevoPedido = async () => {

        const { id: clienteId } = cliente;

        // Remover lo no deseado de productos
        const pedido = productos.map(({ existencia, __typename, creado, ...producto }) => producto);

        try {
            const { data } = await nuevoPedido({
                variables: {
                    input: {
                        cliente: clienteId,
                        total,
                        pedido
                    }
                }
            });
            
            router.push('/pedidos');

            Swal.fire(
                'Correcto',
                'El pedido se registró correctamente',
                'success'
            )
        } catch (error) {
            setMensaje(error.message.replace('GraphQL error: ', ''));
        
            setTimeout(() => {
                setMensaje(null);
            }, 3000)
        
        }
    }

    const mostrarMensaje = () => {
        return (
            <div className="bg-white py-2 px-3 w-full my-3 max-w-sm text-center mx-auto">
                <p>{mensaje}</p>
            </div>
        )
    }

    return (
        <Layout>
            <h1 className="text-2xl text-gray-800 font-light">Crear nuevo pedido</h1>

            { mensaje && mostrarMensaje() }

            <div className="flex justify-center mt-5">
                <div className="w-full max-w-lg">
                    <AsignarCliente />
                    <AsignarProducto />
                    <ResumenPedido />
                    <Total />
                    <button
                        type="button"
                        className={` bg-gray-800 w-full mt-5 p-2 text-white uppercase font-bold hover:bg-gray-900 ${ validarPedido() }`}
                        onClick={ () => crearNuevoPedido() }
                    >
                        Registrar Pedido
                    </button>
                </div>
            </div>
        </Layout>      
    )
}

export default NuevoPedido;
