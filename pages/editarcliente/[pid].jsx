import React from 'react';
import { useRouter } from "next/router";
import Layout from '../../components/Layout';
import { gql, useQuery, useMutation } from '@apollo/client';
import { Formik } from "formik";
import * as Yup from 'yup';
import Swal from 'sweetalert2';
import MensajeError from '../../components/MensajeError';
import { OBTENER_CLIENTE, OBTENER_CLIENTES_USUARIO } from "../../graphql/queries";
import { ACTUALIZAR_CLIENTE } from "../../graphql/mutations";


const EditarCliente = () => {
    const router = useRouter();
    const { query: { id } } = router;

    const {data, loading, error } = useQuery(OBTENER_CLIENTE, {
        variables: {
            id
        }
    });

    const [ actualizarCliente ] = useMutation(ACTUALIZAR_CLIENTE, {
        update(cache, {data}) {
            if(!data) return

            try {
                const { actualizarCliente: clienteActualizado } = data;
                const {obtenerClientesVendedor: clientesActuales} = cache.readQuery({query: OBTENER_CLIENTES_USUARIO});
                
                cache.writeQuery({
                    query: OBTENER_CLIENTES_USUARIO,
                    data: {
                        obtenerClientesVendedor: [...clientesActuales, clienteActualizado]
                    }
                })

                cache.writeQuery({
                    query: OBTENER_CLIENTE,
                    variables: { id },
                    data: {
                        obtenerCliente: clienteActualizado
                    }
                }) 
                
            } catch (error) {
                console.log(error);
            }
        }
    });

    const schemaValidacion = Yup.object({
        nombre: Yup.string()
                    .required('El nombre del cliente es obligatorio'),
        apellido: Yup.string()
                    .required('El apellido del cliente es obligatorio'),
        empresa: Yup.string()
                    .required('La empresa del cliente es obligatoria'),
        email: Yup.string()
                    .required('El email del cliente es obligatorio')
                    .email('El email no es válido'),
    });

    if(loading) return 'Cargando...'
    
    const { obtenerCliente } = data;

    const actualizarInfoCliente = async (valores) => {
        const { nombre, apellido, email, empresa, telefono } = valores;

        try {
            const { data } = await actualizarCliente({
                variables: {
                    id,
                    input: {
                        nombre, 
                        apellido,
                        email,
                        empresa,
                        telefono
                    }
                }
            })

            Swal.fire(
                'Actualizado',
                'El cliente se actualizó correctamente',
                'success'
            );
            
            router.push('/');
        } catch (error) {
            console.log(error);
        }
    }


    return (
        <Layout>
            <h1 className="text-2xl text-gray-800 font-light">Editar Cliente</h1>

            <div className="flex justify-center mt-5">
                <div className="w-full max-w-lg">

                    <Formik 
                        validationSchema={ schemaValidacion }
                        enableReinitialize
                        initialValues={ obtenerCliente }
                        onSubmit={( valores ) => {
                            actualizarInfoCliente(valores)
                        }}
                    >

                        { props => {
                            return (
                                <form 
                                    className="bg-white shadow-md px-8 pt-6 pb-8 mb-4"
                                    onSubmit={props.handleSubmit}
                                >
                                    <div className="mb-4">
                                        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nombre">
                                            Nombre
                                        </label>
                                        <input 
                                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                            id="nombre"
                                            type="text"
                                            placeholder="Nombre Cliente"
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            value={props.values.nombre} 
                                        />
                                    </div>

                                    { props.touched.nombre && props.errors.nombre ? (
                                        <MensajeError mensaje={props.errors.nombre}/>
                                    ): null }

                                    <div className="mb-4">
                                        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="apellido">
                                            Apellido
                                        </label>
                                        <input 
                                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                            id="apellido"
                                            type="text"
                                            placeholder="Apellido Cliente"
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            value={props.values.apellido} 
                                        />
                                    </div>

                                    {props.touched.apellido && props.errors.apellido ? (
                                        <MensajeError mensaje={props.errors.apellido}/>
                                    ): null }

                                    <div className="mb-4">
                                        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="empresa">
                                            Empresa
                                        </label>
                                        <input 
                                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                            id="empresa"
                                            type="text"
                                            placeholder="Empresa Cliente"
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            value={props.values.empresa} 
                                        />
                                    </div>

                                    { props.touched.empresa && props.errors.empresa ? (
                                        <MensajeError mensaje={props.errors.empresa}/>
                                    ): null} 

                                    <div className="mb-4">
                                        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">
                                            Email
                                        </label>
                                        <input 
                                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                            id="email"
                                            type="email"
                                            placeholder="Email Cliente"
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            value={props.values.email} 
                                        />
                                    </div>

                                    { props.touched.email && props.errors.email ? (
                                        <MensajeError mensaje={props.errors.email}/>
                                    ): null}

                                    <div className="mb-4">
                                        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="telefono">
                                            Teléfono
                                        </label>
                                        <input 
                                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:shadow-outline"
                                            id="telefono"
                                            type="tel"
                                            placeholder="Teléfono Cliente"
                                            onChange={props.handleChange}
                                            onBlur={props.handleBlur}
                                            value={props.values.telefono}
                                        />
                                    </div>

                                    <input
                                        type="submit"
                                        className="bg-gray-800 w-full mt-5 p-2 text-white uppercase font-bold hover:bg-gray-900"
                                        value="Editar Cliente" 
                                    />
                                </form>
                            )
                        }}
                    </Formik>
                </div>
            </div>

        </Layout>
    )
}

export default EditarCliente
