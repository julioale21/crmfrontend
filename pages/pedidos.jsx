import React from 'react';
import Layout from '../components/Layout';
import Pedido from '../components/pedidos/Pedido';
import Link from 'next/link';
import { useQuery } from "@apollo/client";
import { OBTENER_PEDIDOS } from "../graphql/queries";

const Pedidos = () => {

  const { data, loading, error } = useQuery(OBTENER_PEDIDOS);

  if (loading) return 'Cargando...'

  const { obtenerPedidosVendedor } = data;

  return (
      <div>
        <Layout>
          <h1 className="text-2xl text-gray-800 font-light">Pedidos</h1>

          <Link href="/nuevopedido">
            <a className="bg-blue-800 py-2 px-5 mt-3 mb-3 inline-block text-white rounded text-sm hover:bg-gray-800 uppercase font-bold w-full lg:w-auto text-center">Nuevo Pedido</a>
          </Link>

          { obtenerPedidosVendedor.length === 0 ? (
            <p className="mt-5 text-center text-2xl">No hay pedidos cargados</p>
          ) : (
            obtenerPedidosVendedor.map( pedido => (
              <Pedido 
                key={pedido.id}
                pedido={pedido} 
              />
            ))
          ) }
        </Layout>
      </div>
  );
}

export default Pedidos;